package cz.muni.fi.pv168.prison.backend;

import cz.muni.fi.pv168.common.DBUtils;
import cz.muni.fi.pv168.common.IllegalEntityException;
import cz.muni.fi.pv168.common.ServiceFailureException;
import cz.muni.fi.pv168.common.ValidationException;

import javax.sql.DataSource;
import java.sql.*;
import java.time.Clock;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class implements sentence manager.
 */
public class SentenceManagerImpl implements SentenceManager {

    private static final Logger logger = Logger.getLogger(
            CellManagerImpl.class.getName());
    private DataSource dataSource;
    private Clock clock;

    public SentenceManagerImpl(Clock clock) {
        this.clock = clock;
    }

    public SentenceManagerImpl(DataSource ds) {this.dataSource = ds; }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    private void checkDataSource() {
        if (dataSource == null) {
            throw new IllegalStateException("DataSource is not set");
        }
    }



    @Override
    public void createSentence(Sentence sentence) throws ServiceFailureException {
        checkDataSource();
        validate(sentence);
        checkCellCapacityByCell(sentence);
        Connection connection = null;
        PreparedStatement st = null;

        try {
            connection = dataSource.getConnection();
            connection.setAutoCommit(false);
            st = connection.prepareStatement(
                    "INSERT INTO sentence (prisonerId, cellId, startDay, endDay, punishment) VALUES (?,?,?,?,?)");
            st.setLong(1, sentence.getPrisonerId());
            st.setLong(2, sentence.getCellId());
            st.setDate(3, toSqlDate(sentence.getStartDay()));
            st.setDate(4, toSqlDate(sentence.getEndDay()));
            st.setString(5, sentence.getPunishment());

            int count = st.executeUpdate();
            DBUtils.checkUpdatesCount(count, sentence, true);
            connection.commit();

        }catch (SQLException ex) {
            String msg = "Error when inserting sentence into db";
            logger.log(Level.SEVERE, msg, ex);
            throw new ServiceFailureException(msg, ex);
        }finally {
            DBUtils.doRollbackQuietly(connection);
            DBUtils.closeQuietly(connection,st);
        }


    }

    @Override
    public List<Sentence> findAllSentences() throws ServiceFailureException {
        checkDataSource();
        Connection connection = null;
        PreparedStatement st = null;

        try {
            connection = dataSource.getConnection();
            st = connection.prepareStatement(
                    "SELECT prisonerId, cellId, startDay, endDay, punishment FROM sentence");
            return executeQueryFromMultipleSentence(st);

        }catch (SQLException ex) {
            String msg = "Error when getting sentences from db";
            logger.log(Level.SEVERE, msg, ex);
            throw new ServiceFailureException(msg, ex);
        }finally {
            DBUtils.closeQuietly(connection, st);
        }

    }


    @Override
    public void updateSentence(Sentence oldSentence, Sentence newSentence) throws ServiceFailureException {
        checkDataSource();
        validate(oldSentence);
        validate(newSentence);
        if (oldSentence.getCellId() != newSentence.getCellId()) {
            checkCellCapacityByCell(newSentence);
        }

        Connection connection = null;
        PreparedStatement st = null;

        try {
            connection = dataSource.getConnection();
            connection.setAutoCommit(false);
            st = connection.prepareStatement(
                    "UPDATE sentence SET prisonerId = ?, cellId = ?, startDay = ?, endDay = ?, punishment = ? " +
                            "WHERE prisonerId = ? AND cellId = ? AND startDay = ? AND endDay = ?");
            st.setLong(1, newSentence.getPrisonerId());
            st.setLong(2, newSentence.getCellId());
            st.setDate(3, toSqlDate(newSentence.getStartDay()));
            st.setDate(4, toSqlDate(newSentence.getEndDay()));
            st.setString(5, newSentence.getPunishment());

            st.setLong(6, oldSentence.getPrisonerId());
            st.setLong(7, oldSentence.getCellId());
            st.setDate(8, toSqlDate(oldSentence.getStartDay()));
            st.setDate(9, toSqlDate(oldSentence.getEndDay()));

            int count = st.executeUpdate();
            DBUtils.checkUpdatesCount(count, newSentence, false);
            connection.commit();

        }catch(SQLException ex) {
            String msg = "Error when updating sentence in db";
            logger.log(Level.SEVERE, msg, ex);
            throw new ServiceFailureException(msg, ex);
        }finally {
            DBUtils.doRollbackQuietly(connection);
            DBUtils.closeQuietly(connection,st);
        }
    }


    @Override
    public void deleteSentence(Sentence sentence) throws ServiceFailureException {
        checkDataSource();
        validate(sentence);

        Connection connection = null;
        PreparedStatement st = null;

        try {
            connection = dataSource.getConnection();
            connection.setAutoCommit(false);
            st = connection.prepareStatement(
                    "DELETE FROM sentence WHERE prisonerId = ? AND cellId = ? AND startDay = ? AND endDay = ?");
            st.setLong(1, sentence.getPrisonerId());
            st.setLong(2, sentence.getCellId());
            st.setDate(3, toSqlDate(sentence.getStartDay()));
            st.setDate(4, toSqlDate(sentence.getEndDay()));

            int count = st.executeUpdate();
            DBUtils.checkUpdatesCount(count, sentence, false);
            connection.commit();

        }catch (SQLException ex) {
            String msg = "Error when deleting sentence from db";
            logger.log(Level.SEVERE, msg, ex);
            throw new ServiceFailureException(msg, ex);
        }finally {
            DBUtils.doRollbackQuietly(connection);
            DBUtils.closeQuietly(connection, st);
        }
    }

    @Override
    public List<Sentence> findSentencesForPrisoner(Long prisonerId) throws ServiceFailureException {
        checkDataSource();
        if (prisonerId == null) {
            throw new IllegalArgumentException("Error, prisoner is null");
        }

        Connection connection = null;
        PreparedStatement st = null;

        try {
            connection = dataSource.getConnection();
            st = connection.prepareStatement(
                    "SELECT prisonerId, cellId, startDay, endDay, punishment FROM sentence " +
                            "WHERE prisonerId = ?");
            st.setLong(1, prisonerId);
            return executeQueryFromMultipleSentence(st);


        }catch (SQLException ex) {
            String msg = "Error when getting sentences with prisonerId = " + prisonerId + " from db";
            logger.log(Level.SEVERE, msg, ex);
            throw new ServiceFailureException(msg, ex);
        }finally {
            DBUtils.closeQuietly(connection,st);
        }
    }

    @Override
    public List<Sentence> findSentencesForCell(Long cellId) throws ServiceFailureException {
        checkDataSource();
        if (cellId == null) {
            throw new IllegalArgumentException("Error, cell is null");
        }

        Connection connection = null;
        PreparedStatement st = null;

        try {
            connection = dataSource.getConnection();
            st = connection.prepareStatement(
                    "SELECT prisonerId, cellId, startDay, endDay, punishment FROM sentence " +
                            "WHERE cellId = ?");
            st.setLong(1, cellId);

            return executeQueryFromMultipleSentence(st);

        }catch (SQLException ex) {
            String msg = "Error when getting sentences with cellId = " + cellId + " from db";
            logger.log(Level.SEVERE, msg, ex);
            throw new ServiceFailureException(msg, ex);
        }finally {
            DBUtils.closeQuietly(connection,st);
        }

    }



    @Override
    public int findFreeCapacityFromDateToDate(Long cellId, LocalDate from, LocalDate to) throws ServiceFailureException {
        if (cellId == null) {
            throw new IllegalArgumentException("Error, cell is null");
        }

        List<Sentence> list;
        list = findSentencesForCell(cellId);

        Connection connection = null;
        PreparedStatement st = null;

        try {
            connection = dataSource.getConnection();
            st = connection.prepareStatement(
                    "SELECT capacity FROM Cell WHERE Id = ?");
            st.setLong(1, cellId);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                int ret = rs.getInt("capacity");
                ret -= getCurrentSentenceFromList(list, from, to).size();
                return ret;
            } else {
                throw new ServiceFailureException("cell with cellId: " + cellId + " is not in database");
            }

        }catch (SQLException ex) {
            String msg = "Error when getting sentences with cellId = " + cellId + " from db";
            logger.log(Level.SEVERE, msg, ex);
            throw new ServiceFailureException(msg, ex);
        }finally {
            DBUtils.closeQuietly(connection,st);
        }

    }


    public List<Cell> findEmptyCells() {
        List<Sentence> list = findAllSentences();
        List<Sentence> currentSentences = getCurrentSentenceFromList(list, LocalDate.now(clock), LocalDate.now(clock));
        CellManagerImpl cm = new CellManagerImpl();
        cm.setDataSource(dataSource);
        List<Cell> allCells = cm.findAllCells();
        Cell cellToDelete;
        for (Sentence s :currentSentences) {
            cellToDelete = cm.getCellById(s.getCellId());
            allCells.remove(cellToDelete);
        }
        return allCells;

    }


    private void validate(Sentence sentence) {
        if (sentence == null) {
            throw new IllegalArgumentException("Error, sentence is null");
        }
        if (sentence.getPrisonerId() == null) {
            throw new ValidationException("Error, prisonerId in sentence is null");
        }
        if (sentence.getCellId() == null) {
            throw new ValidationException("Error, cellId in sentence is null");
        }


        if (sentence.getStartDay() == null) {
            throw new ValidationException("Error, startDay in sentence is null");
        }
        if (sentence.getEndDay() == null) {
            throw new ValidationException("Error, endDay in sentence is null");
        }
        if (sentence.getStartDay().isAfter(sentence.getEndDay())) {
            throw new ValidationException("Error, startDay  is before endDay in sentence ");
        }
    }

    private void checkCellCapacityByCell(Sentence sentence) {
        int n = findFreeCapacityFromDateToDate(sentence.getCellId(), sentence.getStartDay(), sentence.getEndDay());
        if (n <= 0) {
            throw new ValidationException("free cell capacity is 0");
        }
    }



    static List<Sentence> executeQueryFromMultipleSentence(PreparedStatement st) throws SQLException{
        ResultSet rs = st.executeQuery();
        List<Sentence> list = new ArrayList<>();

        while (rs.next()) {
            list.add(rowToSentence(rs));
        }
        return list;
    }

    static Sentence executeQueryForSingleSentence(PreparedStatement st) throws SQLException, ServiceFailureException{
        ResultSet rs = st.executeQuery();
        if (rs.next()) {
            Sentence sentence = rowToSentence(rs);
            if (rs.next()) {
                throw new ServiceFailureException("Error, more sentences with same id found");
            }
            return sentence;
        }else{
            return null;
        }

    }


    static private Sentence rowToSentence(ResultSet rs) throws SQLException {
        Sentence sentence = new Sentence();
        sentence.setPrisonerId(rs.getLong("prisonerId"));
        sentence.setCellId(rs.getLong("cellId"));
        sentence.setStartDay(toLocalDate(rs.getDate("startDay")));
        sentence.setEndDay(toLocalDate(rs.getDate("endDay")));
        sentence.setPunishment(rs.getString("punishment"));
        return sentence;
    }

    private List<Sentence> getCurrentSentenceFromList(List<Sentence> list, LocalDate from, LocalDate to) {
        List<Sentence> retList = new ArrayList<>();

        for (Sentence s : list) {
            if (s.getStartDay().isAfter(from) && s.getStartDay().isBefore(to)){
                retList.add(s);
            }else if (s.getEndDay().isAfter(from) && s.getEndDay().isBefore(to)) {
                retList.add(s);
            }else if (s.getStartDay().isEqual(from)
                    || s.getStartDay().isEqual(to)
                    || s.getEndDay().isEqual(from)
                    || s.getEndDay().isEqual(to)) {
                retList.add(s);
            }
        }
        return retList;
    }

    private static LocalDate toLocalDate(Date date) {
        return date == null ? null : date.toLocalDate();
    }

    private static Date toSqlDate(LocalDate localDate) {
        return localDate == null ? null : Date.valueOf(localDate);
    }

}
