package cz.muni.fi.pv168.swing.gui;

import javax.swing.*;

/**
 * Created by olda on 28.04.2016.
 */
public class PrisonerDatabaseWindow {
    private JPanel prisonerDatabasePanel;
    private JPanel prisonerDatabaseLeftPanel;
    private JRadioButton onlyCurrentPrisoners;
    private JList prisonersList;
    private JPanel prisonerInfoPanel;
    private JLabel prisonerName;
    private JLabel prisonerSurname;
    private JLabel prisonerBorn;
    private JPanel prisonerInfoButtonPanel;
    private JButton prisonerDeleteButton;
    private JButton prisonerUpdateButton;
    private JLabel prisonerNameValue;
    private JLabel prisonerSurnameValue;
    private JLabel prisonerBornValue;
    private JPanel sentenceInfoPanel;
    private JScrollBar prisonersListScrollBar;
    private JList sentenceList;
    private JPanel sentenceListPanel;
    private JLabel sentenceStartDay;
    private JLabel sentenceEndDay;
    private JLabel sentencePunsihment;
    private JLabel sentenceCellId;
    private JPanel sentenceInfoButtonPanel;
    private JButton sentenceDeleteButton;
    private JButton sentenceUpdateButton;
    private JButton goToCellButton;
    private JButton cancelButton;
    private JLabel sentenceStartDayValue;
    private JLabel sentenceEndDayValue;
    private JLabel sentenceBornValue;
    private JLabel sentenceCellIdValue;
}
