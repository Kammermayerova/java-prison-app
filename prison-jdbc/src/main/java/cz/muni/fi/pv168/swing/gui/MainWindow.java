package cz.muni.fi.pv168.swing.gui;

import javax.swing.*;
import java.awt.event.ActionListener;

/**
 * Created by olda on 27.04.2016.
 */
public class MainWindow {


    private JPanel mainWindowRoot;
    private JPanel prisonerAndCellPanel;
    private JLabel numOfPrisoners;
    private JLabel numOfCells;
    private JButton prisonerDatabaseButton;
    private JButton cellDatabaseButton;
    private JPanel addSentencePanel;
    private JLabel createNewSentence;
    private JLabel choosePrisoner;
    private JLabel chooseCell;
    private JButton createNewPrisonerButton;
    private JButton createNewCellButton;
    private JList choosePrisonerList;
    private JList chooseCellList;
    private JScrollBar choosePrisonerScrollBar;
    private JScrollBar chooseCellScrollBar;
    private JLabel sentenceFrom;
    private JLabel sentenceTo;
    private JLabel sentenceInfo;
    private JTextField sentencePunishmentText;
    private JButton sentenceDatabaseButton;
    private JPanel sentenceFromPanel;
    private JPanel sentenceToPanel;
    private JComboBox sentenceFromYearComboBox;
    private JComboBox sentenceFromMonthComboBox;
    private JComboBox sentenceFromDayComboBox;
    private JComboBox sentnecneToYearComboBox;
    private JComboBox sentnecneToMonthComboBox;
    private JComboBox sentnecneToDayComboBox;
    private JButton createSentenceButton;
    private JLabel numOfPrisonersValue;
    private JLabel numOfCellsValue;
    private JLabel numOfPrisonersCurrent;
    private JLabel numOfPrisonersCurrentValue;
    private JLabel numOfFreeSpots;
    private JLabel numOfFreeSpotsValue;
}
