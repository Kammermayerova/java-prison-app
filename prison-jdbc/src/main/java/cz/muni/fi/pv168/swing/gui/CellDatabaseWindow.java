package cz.muni.fi.pv168.swing.gui;

import javax.swing.*;

/**
 * Created by olda on 28.04.2016.
 */
public class CellDatabaseWindow {
    private JPanel cellDatabasePanel;
    private JPanel chooseCellPanel;
    private JPanel operateCellPanel;
    private JRadioButton onlyFreeCellsRadioButton;
    private JList cellsList;
    private JScrollBar cellsListScrollBar;
    private JRadioButton onlyCurrentPrisonersRadioButton;
    private JList prisonersInCellList;
    private JPanel cellInfoPanel;
    private JScrollBar prisonersInCellListScrollBar;
    private JPanel prisonersInfoPanel;
    private JLabel cellId;
    private JLabel cellFloor;
    private JLabel cellCapacity;
    private JButton cellDeleteButton;
    private JLabel cellIdValue;
    private JLabel cellFloorValue;
    private JLabel cellCapacityValue;
    private JLabel prisonerName;
    private JLabel prisonerSurname;
    private JLabel prisonerBorn;
    private JPanel prisonerInfoButtonPanel;
    private JButton prisonerDeleteButton;
    private JButton prisonerInfoUpdateButton;
    private JLabel prisonerNameValue;
    private JLabel prisonerSurnameValue;
    private JLabel prisonerBornValue;
    private JPanel chooseSentencePanel;
    private JPanel sentenceInfoPanel;
    private JRadioButton onlyCurrentSentenceRadioButton;
    private JList sentenceList;
    private JScrollBar sentenceListScrollBar;
    private JLabel sentenceStartDay;
    private JLabel sentenceEndDay;
    private JLabel sentenceInfo;
    private JPanel sentenceButtonPanel;
    private JLabel sentenceStartDayValue;
    private JLabel sentenceEndDayValue;
    private JLabel sentenceInfoValue;
    private JButton sentenceDeleteButton;
    private JButton sentenceUpdateButton;
    private JButton cancelButton;
}
