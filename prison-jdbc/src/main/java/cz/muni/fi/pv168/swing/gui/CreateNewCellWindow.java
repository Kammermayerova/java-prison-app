package cz.muni.fi.pv168.swing.gui;

import javax.swing.*;

/**
 * Created by olda on 28.04.2016.
 */
public class CreateNewCellWindow {
    private JPanel buttonPanel;
    private JButton createButton;
    private JButton cancelButton;
    private JLabel createCellFloor;
    private JLabel createCellCapacity;
    private JComboBox createCellFloorComboBox;
    private JComboBox createCellCapacityComboBox;
}
