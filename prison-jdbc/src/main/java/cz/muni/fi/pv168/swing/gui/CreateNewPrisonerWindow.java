package cz.muni.fi.pv168.swing.gui;

import javax.swing.*;

/**
 * Created by olda on 27.04.2016.
 */
public class CreateNewPrisonerWindow {
    private JPanel buttonPanel;
    private JButton cancelButton;
    private JButton createButton;
    private JPanel formPanel;
    private JLabel prisonerName;
    private JLabel prisonerSurname;
    private JLabel prisonerBorn;
    private JPanel prisonerBornDatePanel;
    private JComboBox prisonerBornYearComboBox;
    private JComboBox prisonerBornMonthComboBox;
    private JComboBox prisonerBornDayComboBox;
    private JFormattedTextField prisonerSurNameFormattedText;
    private JFormattedTextField prisonerNameFormattedText;
}
